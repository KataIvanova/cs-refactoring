import math
import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1 = (3 + x) / (1 + x^2 * |x - tan(x)|)
Z2 = (x^2 + 3) / (x^2 - 2)
Z3 = (|Z2 / Z1|)^1/2
'''


def main():
    try:
        x = float(input("Введіть значення x: "))
        d = 1 + (x ** 2) * abs(x - math.tan(x))
        if 1 + (x ** 2) * abs(x - math.tan(x)) != 0:
            z1 = (3 + x) / d
            d = (x ** 2) - 2
            if (x ** 2) - 2 != 0:
                z2 = (x ** 2 + 3) / d
                if (3 + x) / 1 + (x ** 2) * abs(x - math.tan(x)) != 0:
                    z3 = math.sqrt(abs(((x ** 2 + 3) / (x ** 2) - 2) / ((3 + x) / 1 + (x ** 2) * abs(x - math.tan(x)))))
                    print(f"Z1 = {z1}")
                    print(f"Z2 = {z2}")
                    print(f"Z3 = {z3}")
                    sys.exit(0)
                else:
                    print("Некоректні дані. Значення Z1 не може бути рівним нулю.")
                    sys.exit(1)
            else:
                print("Некоректні дані. Знаменник не може дорівнювати нулю.")
                sys.exit(1)
        else:
            print("Некоректні дані. Знаменник не може дорівнювати нулю.")
            sys.exit(1)
    except ValueError:
        print("Некоректні дані. Введіть числове значення для x.")
        sys.exit(1)


if __name__ == "__main__":
    main()
