import math
import sys

'''
Обчислити довжину гіпотенузи прямокутного трикутника, якщо задано
довжини катетів.
'''


def main():
    try:
        a = float(input("Введіть довжину першого катета: "))
        b = float(input("Введіть довжину другого катета: "))
        if a > 0:
            if b > 0:
                c = math.sqrt(a ** 2 + b ** 2)
                h = c
            else:
                h = "Некоректні дані. Довжини катетів повинні бути додатніми."
        else:
            h = "Некоректні дані. Довжини катетів повинні бути додатніми."
        if isinstance(h, float):
            print(f"Довжина гіпотенузи: {h}")
            sys.exit(0)
        else:
            print(h)
            sys.exit(1)
    except ValueError:
        print("Некоректні дані. Введіть числові значення для довжин катетів.")
        sys.exit(1)


if __name__ == "__main__":
    main()
